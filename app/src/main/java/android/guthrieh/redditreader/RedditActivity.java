package android.guthrieh.redditreader;

import android.support.v4.app.Fragment;

public class RedditActivity extends SingleFragmentActivity {

    @Override
    protected Fragment creatFragment() {
        return new RedditListFragment();
    }

}
