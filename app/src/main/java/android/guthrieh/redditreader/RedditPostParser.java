package android.guthrieh.redditreader;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class RedditPostParser {

    private static RedditPostParser parser;

    private RedditPostParser() {

    }

    public static  RedditPostParser getInstance() {
        if(parser == null) {
            parser = new RedditPostParser();
        }
        return parser;
    }

    public JSONObject parseInputStream(InputStream inputStream) {
        BufferedReader streamReader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder stringBuilder = new StringBuilder();
        JSONObject jsonObject = null;

        String currentLine;

        try {
            while((currentLine = streamReader.readLine()) != null) {
                stringBuilder.append(currentLine);
            }
            JSONTokener jsonTokener = new JSONTokener(stringBuilder.toString());
            jsonObject = new JSONObject(jsonTokener);
        }
        catch (IOException error) {
            Log.e("RedditPostParser", "IOException (parseInputStream):" + error);
        }
        catch (JSONException error) {
            Log.e("RedditPostParser", "JSONException (parseInputStream):" + error);
        }

        return jsonObject;
    }
}
